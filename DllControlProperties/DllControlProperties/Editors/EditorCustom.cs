﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using DllControlProperties.Models.Local;

namespace DllControlProperties.Editors
{
    public class EditorCustom : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(EndPointConnection), "EndPointEditorKey"},
            { typeof(CommonLocal), "CommonLocalEditorKey"},
            { typeof(Statistic), "StatisticEditorKey"}
        };

        public EditorCustom(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/DllControlProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }
    }
}
