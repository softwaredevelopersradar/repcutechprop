﻿using System.ComponentModel;

namespace DllControlProperties.Models
{
    public enum Languages : byte
    {
        [Description("Русский")]
        RU,
        [Description("English")]
        EN
    }

    public enum AccessTypes : byte
    {
        Admin,
        User
    }
}
