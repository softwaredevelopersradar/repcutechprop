﻿namespace DllControlProperties.Models.Local
{

    public class CommonLocal : AbstractBaseProperties<CommonLocal>
    {
        #region IModelMethods

        public override bool EqualTo(CommonLocal model)
        {
            return Language == model.Language
                && Access == model.Access
                && PathFileReal == model.PathFileReal
                && PathFileImagine == model.PathFileImagine;
        }

        public override CommonLocal Clone()
        {
            return new CommonLocal
            {
                Language = Language,
                Access = Access,
                PathFileReal = PathFileReal,
                PathFileImagine = PathFileImagine
            };
        }

        public override void Update(CommonLocal model)
        {
            Language = model.Language;
            Access = model.Access;
            PathFileReal = model.PathFileReal;
            PathFileImagine = model.PathFileImagine;
        }

        #endregion

        private Languages _language;
        private AccessTypes _access;
        private string _pathFileReal;
        private string _pathFileImagine;

        public Languages Language
        {
            get => _language;
            set
            {
                if (_language == value) return;
                _language = value;
                OnPropertyChanged();
            }
        }

        public AccessTypes Access
        {
            get => _access;
            set
            {
                if (_access == value) return;
                _access = value;
                OnPropertyChanged();
            }
        }

        public string PathFileReal
        {
            get => _pathFileReal;
            set
            {
                if (_pathFileReal == value) return;
                _pathFileReal = value;
                OnPropertyChanged();

            }
        }

        public string PathFileImagine
        {
            get => _pathFileImagine;
            set
            {
                if (_pathFileImagine == value) return;
                _pathFileImagine = value;
                OnPropertyChanged();
            }
        }
    }
}
