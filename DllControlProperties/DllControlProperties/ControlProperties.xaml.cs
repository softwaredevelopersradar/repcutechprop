﻿using DllControlProperties.Models;
using DllControlProperties.Models.Local;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace DllControlProperties
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ControlProperties : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<LocalProperties> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<Languages> OnLanguageChanged = (sender, language) => { };
        #endregion

        #region Properties


        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private LocalProperties savedLocal;

        public LocalProperties Local
        {
            get => (LocalProperties)Resources["localProperties"];
            set
            {
                if ((LocalProperties)Resources["localProperties"] == value)
                {
                    savedLocal = value.Clone();
                    return;
                }
                ((LocalProperties)Resources["localProperties"]).Update(value);
                savedLocal = value.Clone();
            }
        }

        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryLocalNames();
        }
        private void SetDynamicResources(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/DllControlProperties;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/DllControlProperties;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/DllControlProperties;component/Languages/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //TODO
            }

        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Local.Common.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                    if (TranslateDic.ContainsKey(nameCategory))
                        PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        if (!string.IsNullOrWhiteSpace(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name]))
                        {
                            error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                            isvalid = false;
                        }
                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            funcValidation(Local);

        }

        #endregion

        public ControlProperties()
        {
            InitializeComponent();
            InitLocalProperties();
            ChangeLanguage(savedLocal.Common.Language);
            Validate();
        }

        private void InitLocalProperties()
        {
            savedLocal = Local.Clone();
            savedLocal.Common.PropertyChanged += CommonLocal_PropertyChanged;
            Local.Common.PropertyChanged += CheckLanguage_PropertyChanged;
            Local.OnPropertyChanged += OnPropertyChanged;

            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Common), typeof(LocalProperties), typeof(CommonLocal)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Statistic), typeof(LocalProperties), typeof(Statistic)));
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Validate();
        }

        private void CheckLanguage_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CommonLocal.Language))
            {
                OnLanguageChanged(sender, Local.Common.Language);
                ChangeLanguage(Local.Common.Language);
            }

        }

        private void CommonLocal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(CommonLocal.Language):
                    OnLanguageChanged(sender, savedLocal.Common.Language);
                    ChangeLanguage(savedLocal.Common.Language);
                    break;
                default:
                    break;
            }

        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (!savedLocal.EqualTo(Local))
            {
                savedLocal.Update(Local.Clone());
                OnLocalPropertiesChanged(this, savedLocal.Clone());
            }
        }

        private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            if (!savedLocal.EqualTo(Local))
            {
                Local.Update(savedLocal.Clone());
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Validate();
        }

        private void ButDefault_Click(object sender, RoutedEventArgs e)
        {
            OnLocalDefaultButtonClick(this, null);
        }
    }
}
