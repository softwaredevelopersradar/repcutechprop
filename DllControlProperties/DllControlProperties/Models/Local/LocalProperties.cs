﻿using System.ComponentModel;
using DllControlProperties.Models.Local;

namespace DllControlProperties.Models
{
    public class LocalProperties : IModelMethods<LocalProperties>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };
        public LocalProperties()
        {
            Common = new CommonLocal();
            Statistic = new Statistic();

            Common.PropertyChanged += PropertyChanged;
            Statistic.PropertyChanged += PropertyChanged;
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Common))]
        [DisplayName(" ")]
        public CommonLocal Common { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Statistic))]
        [DisplayName(" ")]
        public Statistic Statistic { get; set; }

        #region Model Methods

        public LocalProperties Clone()
        {
            return new LocalProperties
            {
                Common = Common.Clone(),
                Statistic = Statistic.Clone()
            };
        }

        public void Update(LocalProperties localProperties)
        {
            Common.Update(localProperties.Common);
            Statistic.Update(localProperties.Statistic);
        }

        public bool EqualTo(LocalProperties localProperties)
        {
            return Common.EqualTo(localProperties.Common)
                && Statistic.EqualTo(localProperties.Statistic);
        }

        #endregion
    }
}
