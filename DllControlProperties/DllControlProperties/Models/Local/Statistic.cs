﻿namespace DllControlProperties.Models.Local
{
    public class Statistic : AbstractBaseProperties<Statistic>
    {
        #region IModelMethods

        public override bool EqualTo(Statistic model)
        {
            return model.AverageFrequency == AverageFrequency
                   && model.AveragePulseDuration == AveragePulseDuration
                   && model.AveragePulsePeriod == AveragePulsePeriod
                   && model.MaxPulseDuration == MaxPulseDuration
                   && model.MaxPulseFrequency == MaxPulseFrequency
                   && model.MaxPulsePeriod == MaxPulsePeriod
                   && model.MinPulseDuration == MinPulseDuration
                   && model.MinPulseFrequency == MinPulseFrequency
                   && model.MinPulsePeriod == MinPulsePeriod
                   && model.Pulses == Pulses
                   && model.TimeRecord == TimeRecord;
        }

        public override Statistic Clone()
        {
            return new Statistic
            {
                TimeRecord = TimeRecord,
                Pulses = Pulses,
                MinPulsePeriod = MinPulsePeriod,
                MinPulseFrequency = MinPulseFrequency,
                MinPulseDuration = MinPulseDuration,
                MaxPulsePeriod = MaxPulsePeriod,
                MaxPulseFrequency = MaxPulseFrequency,
                MaxPulseDuration = MaxPulseDuration,
                AveragePulsePeriod = AveragePulsePeriod,
                AveragePulseDuration = AveragePulseDuration,
                AverageFrequency = AverageFrequency
            };
        }

        public override void Update(Statistic model)
        {
            AverageFrequency = model.AverageFrequency;
            AveragePulseDuration = model.AveragePulseDuration;
            AveragePulsePeriod = model.AveragePulsePeriod;
            MaxPulseDuration = model.MaxPulseDuration;
            MaxPulseFrequency = model.MaxPulseFrequency;
            MaxPulsePeriod = model.MaxPulsePeriod;
            MinPulseDuration = model.MinPulseDuration;
            MinPulseFrequency = model.MinPulseFrequency;
            MinPulsePeriod = model.MinPulsePeriod;
            Pulses = model.Pulses;
            TimeRecord = model.TimeRecord;
        }

        #endregion

        private float _timeRecord;
        private int _pulses;
        private float _averagePulseDuration;
        private float _minPulseDuration;
        private float _maxPulseDuration;
        private float _averagePulsePeriod;
        private float _minPulsePeriod;
        private float _maxPulsePeriod;
        private float _averageFrequency;
        private float _minPulseFrequency;
        private float _maxPulseFrequency;

        public float TimeRecord
        {
            get => _timeRecord;
            set
            {
                if (_timeRecord == value) return;
                _timeRecord = value;
                OnPropertyChanged();

            }
        }

        public int Pulses
        {
            get => _pulses;
            set
            {
                if (_pulses == value) return;
                _pulses = value;
                OnPropertyChanged();
            }
        }

        public float AveragePulseDuration
        {
            get => _averagePulseDuration;
            set
            {
                if (_averagePulseDuration == value) return;
                _averagePulseDuration = value;
                OnPropertyChanged();
            }
        }

        public float MinPulseDuration
        {
            get => _minPulseDuration;
            set
            {
                if (_minPulseDuration == value) return;
                _minPulseDuration = value;
                OnPropertyChanged();
            }
        }

        public float MaxPulseDuration
        {
            get => _maxPulseDuration;
            set
            {
                if (_maxPulseDuration == value) return;
                _maxPulseDuration = value;
                OnPropertyChanged();

            }
        }

        public float AveragePulsePeriod {
            get => _averagePulsePeriod;
            set
            {
                if (_averagePulsePeriod == value) return;
                _averagePulsePeriod = value;
                OnPropertyChanged();
            }
        }

        public float MinPulsePeriod
        {
            get => _minPulsePeriod;
            set
            {
                if (_minPulsePeriod == value) return;
                _minPulsePeriod = value;
                OnPropertyChanged();

            }
        }

        public float MaxPulsePeriod
        {
            get => _maxPulsePeriod;
            set
            {
                if (_maxPulsePeriod == value) return;
                _maxPulsePeriod = value;
                OnPropertyChanged();
            }
        }

        public float AverageFrequency
        {
            get => _averageFrequency;
            set
            {
                if (_averageFrequency == value) return;
                _averageFrequency = value;
                OnPropertyChanged();
            }
        }

        public float MinPulseFrequency
        {
            get => _minPulseFrequency;
            set
            {
                if (_minPulseFrequency == value) return;
                _minPulseFrequency = value;
                OnPropertyChanged();
            }
        }

        public float MaxPulseFrequency
        {
            get => _maxPulseFrequency;
            set
            {
                if (_maxPulseFrequency == value) return;
                _maxPulseFrequency = value;
                OnPropertyChanged();
            }
        }

    }
}
